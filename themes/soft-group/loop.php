<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>    
        <h2>
            <a href="#"><?php the_title(); ?></a>
        </h2>
        <p class="lead">
            by <a href="index.php"><?php the_author(); ?></a>
        </p>
        <p><span class="glyphicon glyphicon-time"></span> Posted on
            <?php the_date();?>, at <?php the_time();?></p>



        <hr>
        <img class="img-responsive" src="http://placehold.it/900x300" alt="">  <?php  get_the_post_thumbnail(); ?>
        <hr>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore, veritatis, tempora, necessitatibus inventore nisi quam quia repellat ut tempore laborum possimus eum dicta id animi corrupti debitis ipsum officiis rerum.</p>
        <a class="btn btn-primary" href="#">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>

        <hr>
        <hr>
        <img class="img-responsive" src="http://placehold.it/900x300" alt="">  <?php  get_the_post_thumbnail(); ?>
        <hr>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore, veritatis, tempora, necessitatibus inventore nisi quam quia repellat ut tempore laborum possimus eum dicta id animi corrupti debitis ipsum officiis rerum.</p>
        <a class="btn btn-primary" href="#">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>

        <hr>
        <img class="img-responsive" src="http://placehold.it/900x300" alt="">  <?php  get_the_post_thumbnail(); ?>
        <hr>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore, veritatis, tempora, necessitatibus inventore nisi quam quia repellat ut tempore laborum possimus eum dicta id animi corrupti debitis ipsum officiis rerum.</p>
        <a class="btn btn-primary" href="#">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>

        <?php the_content(); ?>


        the_permalink();

        the_excerpt();

        the_post_thumbnail();

        the_time();

    <?php endwhile; ?>
<?php endif; ?>